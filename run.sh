#!/usr/bin/env bash

RUNDIR=/home/stulluk/wine-redalert2

EXE=${RUNDIR}/Ra2.exe

WINEDEBUG=+fps,+timestamp,+d3d wine ${EXE}
